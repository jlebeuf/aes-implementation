#include <stdio.h>
#include <iostream>
#include <ctype.h>
#include <cstdlib>
#include "aes.h"

using namespace std;

struct TestCase {
	int keyLength;
	int plaintextLength;
	int ciphertextLength;
	unsigned char* key;
	unsigned char* plaintext;
	unsigned char* ciphertext;
};

void encrypt(unsigned char* key, int keyLength);
void decrypt(unsigned char* key, int keyLength);
void runTests();
bool test(TestCase testCase);
bool test(unsigned char* key, int keyLength, unsigned char* plaintext, int plaintextLength, unsigned char* expectedCiphertext, int ciphertextLength);
bool test(AES aes, unsigned char* plaintext, int plaintextLength, unsigned char* expectedCiphertext, int ciphertextLength);
bool compareTexts(unsigned char* text1, unsigned char* text2, int length);

int main()
{
	cout << "Justin Lebeuf" << endl << "CSE-178" << endl << "Lab 1" << endl << endl << endl;

	const int bufferLength = 256;
	char input[bufferLength];
	bool quit = false;

	while (!quit)
	{
		cin.clear();
		fflush(stdin);
		cout << endl << endl << "Main Menu ('h' for help)" << endl;
		cin.getline(input, bufferLength);
		char command = tolower(input[0]);

		if (command == 'h')
		{
			cout << "'h' - Display help menu" << endl;
			cout << "'t' - Run hard coded tests" << endl;
			cout << "'e' - Encrypt a HEX string" << endl;
			cout << "'d' - Decrypt a HEX string" << endl;
			cout << "'q' - Quit" << endl;
		}
		else if (command == 't')
		{
			cout << "Running tests" << endl << endl;
			runTests();
		}
		else if (command == 'e' || command == 'd')
		{
			bool flag = true;
			int keyLength;
			unsigned int hex;
			unsigned char key[32]; // Max key length

			// Get key
			flag = true;
			while (flag)
			{
				cout << "Key length (16, 24, or 32 bytes; 0 to cancel): ";
				cin.clear();
				fflush(stdin);
				cin >> keyLength;
				if (keyLength == 0)
				{
					flag = false;
					continue;
				}
				else if (keyLength < 0 || (keyLength != 16 && keyLength != 24 && keyLength != 32))
				{
					cout << "Invalid length" << endl;
					continue;
				}
				else
					flag = false;
			}
				
			cout << "Key: ";
			cin.clear();
			fflush(stdin);
			for (int x = 0; x < keyLength && fscanf_s(stdin, "%2x", &hex) == 1; ++x)
				key[x] = (unsigned char)hex;
			cout << endl;

			if (command == 'e')
			{
				encrypt(key, keyLength);
			}
			else
			{
				decrypt(key, keyLength);
			}
		}
		else if (command == 'q')
		{
			quit = true;
		}
	}
}



void encrypt(unsigned char* key, int keyLength)
{
	int plntxtLength;
	int cphtxtLength;
	unsigned int hex;
	unsigned char* plaintext;
	unsigned char* ciphertext;
	AES aes(keyLength, key);

	// Get plaintext
	bool flag = true;
	while (flag)
	{
		cout << "Plaintext byte length (2 hex chars per byte): ";
		cin.clear();
		fflush(stdin);
		cin >> plntxtLength;
		if (plntxtLength == 0)
		{
			return;
		}
		else if (plntxtLength < 0)
		{
			cout << "Invalid length" << endl;
			continue;
		}
		else
			flag = false;
	}

	plaintext = new unsigned char[plntxtLength];
	cout << "Plaintext (hex): " << endl;
	cin.clear();
	fflush(stdin);
	for (int x = 0; x < plntxtLength && fscanf_s(stdin, "%2x", &hex) == 1; ++x)
		plaintext[x] = (unsigned char)hex;
	cout << endl;

	cphtxtLength = plntxtLength / 16;
	int i = plntxtLength % 16;
	if (i > 0)
		cphtxtLength++;
	cphtxtLength *= 16;
	ciphertext = new unsigned char[cphtxtLength];
	aes.encrypt(plaintext, plntxtLength, ciphertext);
	cout << "Ciphertext: " << endl;
	for (int i = 0; i < cphtxtLength; i++)
		printf("%.2X", ciphertext[i]);
}

void decrypt(unsigned char* key, int keyLength)
{
	int plntxtLength;
	int cphtxtLength;
	unsigned int hex;
	unsigned char* plaintext;
	unsigned char* ciphertext;
	AES aes(keyLength, key);

	// Get ciphertext
	bool flag = true;
	while (flag)
	{
		cout << "Ciphertext byte length (2 hex chars per byte): ";
		cin.clear();
		fflush(stdin);
		cin >> cphtxtLength;
		if (cphtxtLength == 0)
		{
			return;
		}
		else if (cphtxtLength < 0 || (cphtxtLength % 16) != 0)
		{
			cout << "Invalid length, cipher length must be a multiple of 16" << endl;
			continue;
		}
		else
			flag = false;
	}

	ciphertext = new unsigned char[cphtxtLength];
	cout << "Ciphertext (hex): " << endl;
	cin.clear();
	fflush(stdin);
	for (int x = 0; x < cphtxtLength && fscanf_s(stdin, "%2x", &hex) == 1; ++x)
		ciphertext[x] = (unsigned char)hex;
	cout << endl;

	plaintext = new unsigned char[cphtxtLength];
	aes.decrypt(ciphertext, cphtxtLength, plaintext);
	cout << "Plaintext: " << endl;
	for (int i = 0; i < cphtxtLength; i++)
		printf("%.2X", plaintext[i]);
}

void runTests()
{
	// FIPS 197 Appendix C Example 1 (AES-128)
	TestCase test1 = TestCase();
	test1.keyLength = 16;
	test1.plaintextLength = 16;
	test1.ciphertextLength = 16;
	test1.key = new unsigned char[test1.keyLength] {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };
	test1.plaintext = new unsigned char[test1.plaintextLength] {
		0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };
	test1.ciphertext = new unsigned char[test1.ciphertextLength] {
		0x69, 0xc4, 0xe0, 0xd8, 0x6a, 0x7b, 0x04, 0x30, 0xd8, 0xcd, 0xb7, 0x80, 0x70, 0xb4, 0xc5, 0x5a };

	// FIPS 197 Appendix C Example 2 (AES-192)
	TestCase test2 = TestCase();
	test2.keyLength = 24;
	test2.plaintextLength = 16;
	test2.ciphertextLength = 16;
	test2.key = new unsigned char[test2.keyLength] {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
			0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17 };
	test2.plaintext = new unsigned char[test2.plaintextLength] {
		0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };
	test2.ciphertext = new unsigned char[test2.ciphertextLength] {
		0xdd, 0xa9, 0x7c, 0xa4, 0x86, 0x4c, 0xdf, 0xe0, 0x6e, 0xaf, 0x70, 0xa0, 0xec, 0x0d, 0x71, 0x91 };

	// FIPS 197 Appendix C Example 3 (AES-256)
	TestCase test3 = TestCase();
	test3.keyLength = 32;
	test3.plaintextLength = 16;
	test3.ciphertextLength = 16;
	test3.key = new unsigned char[test3.keyLength] {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
			0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f };
	test3.plaintext = new unsigned char[test3.plaintextLength] {
		0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };
	test3.ciphertext = new unsigned char[test3.ciphertextLength] {
		0x8e, 0xa2, 0xb7, 0xca, 0x51, 0x67, 0x45, 0xbf, 0xea, 0xfc, 0x49, 0x90, 0x4b, 0x49, 0x60, 0x89 };

	// Test 4
	TestCase test4 = TestCase();
	test4.keyLength = 32;
	test4.plaintextLength = 19;
	test4.ciphertextLength = 32;
	test4.key = new unsigned char[test4.keyLength] {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
			0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f };
	test4.plaintext = new unsigned char[test4.plaintextLength] {
		0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0,
			0xab, 0xcd, 0xef };
	test4.ciphertext = new unsigned char[test4.ciphertextLength] {
		0xa3, 0x5e, 0x35, 0x76, 0x7d, 0x75, 0xdb, 0xdf, 0x33, 0x4e, 0x20, 0x62, 0x0a, 0xc3, 0xbd, 0x4c,
			0x0b, 0xbd, 0xe8, 0x40, 0x15, 0x49, 0xbb, 0x37, 0xd1, 0xb5, 0x23, 0x9e, 0x36, 0xaa, 0x23, 0x9d };

	// Test 5
	TestCase test5 = TestCase();
	test5.keyLength = 32;
	test5.plaintextLength = 40;
	test5.ciphertextLength = 48;
	test5.key = new unsigned char[test5.keyLength] {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
			0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f };
	test5.plaintext = new unsigned char[test5.plaintextLength] {
		0x11, 0x1a, 0x90, 0x4a, 0x21, 0xd5, 0xb4, 0x6a, 0xf4, 0xa5, 0xa2, 0xca, 0x37, 0x70, 0x3e, 0x5b,
			0xcd, 0xd6, 0x7c, 0x1f, 0xc4, 0x4f, 0xa0, 0x9d, 0xb3, 0xca, 0xcf, 0x4b, 0xca, 0xd1, 0xe3, 0x0f,
			0x62, 0xc4, 0xa0, 0xe7, 0xb2, 0xb2, 0x51, 0x63 };
	test5.ciphertext = new unsigned char[test5.ciphertextLength] {
		0x85, 0x94, 0xde, 0x6b, 0xa5, 0xfa, 0x06, 0x4a, 0x8c, 0x2e, 0xc4, 0x07, 0x5d, 0x52, 0xe9, 0xf8,
			0x31, 0xfc, 0x85, 0x00, 0x69, 0xf6, 0x95, 0x58, 0x3d, 0x7b, 0x82, 0xe9, 0xbe, 0xa4, 0x67, 0x42,
			0x4a, 0xce, 0x30, 0x2e, 0x82, 0x56, 0xd8, 0x98, 0xf2, 0x3e, 0x97, 0x2a, 0x79, 0xdf, 0xc3, 0x80 };

	// TEST 6
	TestCase test6 = TestCase();
	test6.keyLength = 32;
	test6.plaintextLength = 100;
	test6.ciphertextLength = 112;
	test6.key = new unsigned char[test6.keyLength] {
		0xfb, 0xcd, 0xaf, 0x01, 0x8e, 0xc4, 0x9a, 0x70, 0x06, 0xee, 0xe4, 0x85, 0x42, 0xb3, 0xc5, 0x6f,
			0x55, 0xd9, 0x1a, 0x69, 0xb8, 0x04, 0x5b, 0x10, 0x67, 0xcf, 0xc9, 0xec, 0x38, 0xa9, 0x2b, 0x17 };
	test6.plaintext = new unsigned char[test6.plaintextLength] {
		0x0e, 0x26, 0x01, 0x94, 0x82, 0x57, 0x4f, 0xe9, 0xf1, 0xb5, 0x67, 0x3e, 0xd7, 0x1d, 0x54, 0x79,
			0x88, 0xc6, 0x9f, 0x08, 0x9c, 0xf5, 0x4f, 0x69, 0x6d, 0x6b, 0x26, 0x4e, 0x89, 0x92, 0xe1, 0x88,
			0xd6, 0x1b, 0x2c, 0xcd, 0x1f, 0x4b, 0xbf, 0x7b, 0xe8, 0xfe, 0x74, 0xba, 0x90, 0x8e, 0x4b, 0xe9,
			0x43, 0x14, 0x85, 0x53, 0xfe, 0x0c, 0xad, 0x08, 0x77, 0x63, 0x6d, 0x48, 0x36, 0xfa, 0x75, 0xc8,
			0x3f, 0xf3, 0x9c, 0x90, 0xe2, 0xef, 0x4b, 0xfc, 0xc3, 0x3c, 0x70, 0x7d, 0xd0, 0x6d, 0xb9, 0x7d,
			0x8c, 0x6b, 0x43, 0xbb, 0xf6, 0xb9, 0x9d, 0x1a, 0xda, 0xfa, 0xcc, 0x5f, 0x39, 0xfc, 0x9b, 0x96,
			0xca, 0xab, 0xb4, 0x0c };
	test6.ciphertext = new unsigned char[test6.ciphertextLength] {
		0xcf, 0x1d, 0xeb, 0x2c, 0xde, 0x42, 0x29, 0x56, 0x52, 0xbd, 0x58, 0x0c, 0x1f, 0xcf, 0x5c, 0x99,
			0x7a, 0xc8, 0xdf, 0x63, 0x97, 0x19, 0x57, 0xeb, 0x68, 0xdd, 0xea, 0xd9, 0x94, 0xf3, 0xe2, 0x9f,
			0xeb, 0xcc, 0x9c, 0x2f, 0xef, 0x4c, 0x51, 0x8c, 0x7b, 0x78, 0xb8, 0x1d, 0x78, 0xc9, 0xa5, 0x11,
			0x21, 0xf5, 0xf6, 0xd0, 0xd4, 0xb2, 0xe5, 0x99, 0x92, 0x26, 0xfc, 0x9a, 0xdd, 0x83, 0xdb, 0xe2,
			0x8b, 0xfe, 0xf1, 0xa4, 0x48, 0x43, 0x76, 0x0d, 0x75, 0x2e, 0xf3, 0xe4, 0xeb, 0xfe, 0x02, 0xd3,
			0xd4, 0x58, 0xef, 0xf0, 0xef, 0xe1, 0x87, 0xc9, 0x66, 0xc6, 0xb9, 0xd3, 0xee, 0x19, 0xb9, 0xa7,
			0x5c, 0xe5, 0x26, 0x5a, 0xd5, 0x8f, 0x78, 0x15, 0xd0, 0x55, 0x2f, 0x52, 0xe6, 0x55, 0x8a, 0x89 };


	// TEST 1
	cout << "Test 1, FIPS-197 Appendix C Example 1" << endl << endl;
	test(test1);

	// TEST 2
	cout << "Test 2, FIPS-197 Appendix C Example 2" << endl << endl;
	test(test2);

	// TEST 3
	cout << "Test 3, FIPS-197 Appendix C Example 3" << endl << endl;
	test(test3);

	// TEST 4
	cout << "Test 4" << endl << endl;
	test(test4);

	// TEST 5
	cout << "Test 5" << endl << endl;
	test(test5);

	// TEST 6
	cout << "Test 6" << endl << endl;
	test(test6);
}

bool test(TestCase testCase)
{
	return test(testCase.key, testCase.keyLength, testCase.plaintext, testCase.plaintextLength, testCase.ciphertext, testCase.ciphertextLength);
}

bool test(unsigned char* key, int keyLength, unsigned char* plaintext, int plaintextLength, unsigned char* expectedCiphertext, int ciphertextLength)
{
	AES aes = AES(keyLength, key);

	cout << "Running test with";
	cout << endl << "Key:" << endl;
	for (int i = 0; i < keyLength; i++)
		printf("%.2X", key[i]);
	cout << endl;

	return test(aes, plaintext, plaintextLength, expectedCiphertext, ciphertextLength);
}

bool test(AES aes, unsigned char* plaintext, int plaintextLength, unsigned char* expectedCiphertext, int ciphertextLength)
{
	bool failure = false;

	int maxLength = (plaintextLength>ciphertextLength) ? plaintextLength : ciphertextLength;
	unsigned char* actualCiphertext = new unsigned char[maxLength];
	unsigned char* actualPlaintext = new unsigned char[maxLength];

	if (aes.encrypt(plaintext, plaintextLength, actualCiphertext))
	{
		cout << endl << "Encryption:" << endl;

		cout << "Expected cipher and Actual cipher:" << endl;
		for (int i = 0; i < ciphertextLength; i++)
			printf("%.2X", expectedCiphertext[i]);
		cout << endl;
		for (int i = 0; i < ciphertextLength; i++)
			printf("%.2X", actualCiphertext[i]);
		if (compareTexts(expectedCiphertext, actualCiphertext, ciphertextLength))
			cout << endl << "MATCH";
		else
		{
			cout << endl << "NON MATCH";
			failure = true;
		}
		cout << endl;

		cout << endl << "Decryption:" << endl;
		if (aes.decrypt(expectedCiphertext, ciphertextLength, actualPlaintext))
		{
			cout << "Expected plaintext and Actual plaintext:" << endl;
			for (int i = 0; i < plaintextLength; i++)
				printf("%.2X", plaintext[i]);
			cout << endl;
			for (int i = 0; i < plaintextLength; i++)
				printf("%.2X", actualPlaintext[i]);
			if (compareTexts(plaintext, actualPlaintext, plaintextLength))
				cout << endl << "MATCH";
			else
			{
				cout << endl << "NON MATCH";
				failure = true;
			}
			cout << endl;
		}
		else
		{
			cout << "Decryption failed";
			failure = true;
		}
	}
	else
	{
		cout << "Encryption failed";
		failure = true;
	}
	cout << endl << endl;

	return !failure;
}

bool compareTexts(unsigned char* text1, unsigned char* text2, int length)
{
	for (int i = 0; i < length; i++)
		if (text1[i] != text2[i])
			return false;

	return true;
}
