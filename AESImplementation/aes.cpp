// Justin Lebeuf
// CSE-178
// Project 1
// AES Encryption/Decryption

#include "aes.h"


AES::AES()
{
	keyValid = false;
}

AES::AES(int keyLength, unsigned char* key)
{
	keyValid = false;
	setKey(keyLength, key);
}


bool AES::setKey(int keyLength, unsigned char* key)
{
	if (!((keyLength == 16) || (keyLength == 24) || (keyLength == 32)))
		return false;

	this->keyLength = keyLength;
	if (keyLength == 16)
	{
		Nk = 4;
		Nb = 4;
		Nr = 10;
	}
	else
	if (keyLength == 24)
	{
		Nk = 6;
		Nb = 4;
		Nr = 12;
	}
	else
	if (keyLength == 32)
	{
		Nk = 8;
		Nb = 4;
		Nr = 14;
	}
	int i = 0;
	while (i < keyLength)
	{
		this->key[i] = key[i];
		i++;
	}

	expandKey();
	keyValid = true;

	return true;
}


/*
Cipher(byte in[4*Nb], byte out[4*Nb], word w[Nb*(Nr+1)])
begin
byte state[4,Nb]

state = in

AddRoundKey(state, w[0, Nb-1])		// See Sec. 5.1.4

for round = 1 step 1 to Nr�1
SubBytes(state)					// See Sec. 5.1.1
ShiftRows(state)				// See Sec. 5.1.2
MixColumns(state)				// See Sec. 5.1.3
AddRoundKey(state, w[round*Nb, (round+1)*Nb-1])
end for

SubBytes(state)
ShiftRows(state)
AddRoundKey(state, w[Nr*Nb, (Nr+1)*Nb-1])

out = state
end
*/

/// <summary>
/// Encrypts given single block using expanded keys
/// </summary>
/// <param name="in">4*Nb byte state to encrypt</param>
/// <param name="out">4*Nb byte state of encrypted input</param>
/// <param name="keys">4*Nb*(Nr+1) byte array of round key words</param>
/// <returns>Success (out=enc(in)) or failure (out unchanged)</returns>
bool AES::encryptBlock(unsigned char* in, unsigned char* out)
{
	if (!keyValid)
		return false;

	unsigned char* state = new unsigned char[4 * Nb];

	for (int i = 0; i < 4 * Nb; i++)
		state[i] = in[i];

	addRoundKey(state, expandedKey + 0);

	for (int round = 1; round < Nr; round++)
	{
		subBytes(state);
		shiftRows(state);
		mixColumns(state);
		unsigned char* roundKey = expandedKey + (round*Nb*4);
		addRoundKey(state, roundKey);
	}

	subBytes(state);
	shiftRows(state);
	addRoundKey(state, expandedKey + Nr*Nb*4);

	for (int i = 0; i < 4 * Nb; i++)
		out[i] = state[i];

	return true;
}


bool AES::decryptBlock(unsigned char* in, unsigned char* out)
{
	if (!keyValid)
		return false;

	unsigned char* state = new unsigned char[4 * Nb];

	for (int i = 0; i < 4 * Nb; i++)
		state[i] = in[i];

	addRoundKey(state, expandedKey + Nr*Nb*4);

	for (int round = Nr-1; round >= 1; round--)
	{
		invShiftRows(state);
		invSubBytes(state);
		unsigned char* roundKey = expandedKey + (round*Nb * 4);
		addRoundKey(state, roundKey);
		invMixColumns(state);
	}

	invShiftRows(state);
	invSubBytes(state);
	addRoundKey(state, expandedKey + 0);

	for (int i = 0; i < 4 * Nb; i++)
		out[i] = state[i];

	return true;
}


/// <summary>
/// Applies sBox substitution to a given state
/// </summary>
/// <param name="state">State to apply sBox to</param>
void AES::subBytes(unsigned char* state)
{
	for (int i = 0; i < 4 * Nb; i++)
		state[i] = sBox[state[i]];
}

/// <summary>
/// Applies inverse sBox substitution to a given state
/// </summary>
/// <param name="state">State to apply sBox to</param>
void AES::invSubBytes(unsigned char* state)
{
	for (int i = 0; i < 4 * Nb; i++)
		state[i] = sBoxI[state[i]];
}

/// <summary>
/// Performs row shift operation on state
/// </summary>
/// <param name="state">State to apply shift to</param>
void AES::shiftRows(unsigned char* state)
{
	// 0*4+0	1*4+0	2*4+0	3*4+0
	// 0*4+1	1*4+1	2*4+1	3*4+1
	// 0*4+2	1*4+2	2*4+2	3*4+2
	// 0*4+3	1*4+3	2*4+3	3*4+3

	// Row 0 shifts left 0

	// Row 1 shifts left 1
	char temp = state[0 * 4 + 1];
	state[0 * 4 + 1] = state[1 * 4 + 1];
	state[1 * 4 + 1] = state[2 * 4 + 1];
	state[2 * 4 + 1] = state[3 * 4 + 1];
	state[3 * 4 + 1] = temp;

	// Row 2 shifts left 2
	temp = state[0 * 4 + 2];
	state[0 * 4 + 2] = state[2 * 4 + 2];
	state[2 * 4 + 2] = temp;
	temp = state[1 * 4 + 2];
	state[1 * 4 + 2] = state[3 * 4 + 2];
	state[3 * 4 + 2] = temp;

	// Row 3 shifts left 3
	temp = state[0 * 4 + 3];
	state[0 * 4 + 3] = state[3 * 4 + 3];
	state[3 * 4 + 3] = state[2 * 4 + 3];
	state[2 * 4 + 3] = state[1 * 4 + 3];
	state[1 * 4 + 3] = temp;
}

/// <summary>
/// Performs inverse row shift operation on state
/// </summary>
/// <param name="state">State to apply shift to</param>
void AES::invShiftRows(unsigned char* state)
{
	// 0*4+0	1*4+0	2*4+0	3*4+0
	// 0*4+1	1*4+1	2*4+1	3*4+1
	// 0*4+2	1*4+2	2*4+2	3*4+2
	// 0*4+3	1*4+3	2*4+3	3*4+3

	// Row 0 shifts left 0

	// Row 1 shifts left 1
	char temp = state[0 * 4 + 1];
	state[0 * 4 + 1] = state[3 * 4 + 1];
	state[3 * 4 + 1] = state[2 * 4 + 1];
	state[2 * 4 + 1] = state[1 * 4 + 1];
	state[1 * 4 + 1] = temp;

	// Row 2 shifts left 2
	temp = state[0 * 4 + 2];
	state[0 * 4 + 2] = state[2 * 4 + 2];
	state[2 * 4 + 2] = temp;
	temp = state[1 * 4 + 2];
	state[1 * 4 + 2] = state[3 * 4 + 2];
	state[3 * 4 + 2] = temp;

	// Row 3 shifts left 3
	temp = state[0 * 4 + 3];
	state[0 * 4 + 3] = state[1 * 4 + 3];
	state[1 * 4 + 3] = state[2 * 4 + 3];
	state[2 * 4 + 3] = state[3 * 4 + 3];
	state[3 * 4 + 3] = temp;
}

/// <summary>
/// Performs column mix operation
/// </summary>
/// <param name="state">State to mix columns on</param>
void AES::mixColumns(unsigned char* state)
{
	// s'(0,c) = ({02} dot s(0,c) ^ ({03} dot s(1,c)) ^ s(2,c) ^ s(3,c)
	// s'(1,c) = s(0,c) ^ ({02} dot s(1,c)) ^ ({03} dot s(2,c)) ^ s(3,c)
	// s'(2,c) = s(0,c) ^ s(1,c) ^ ({02} dot s(2,c)) ^ ({03} dot s(3,c))
	// s'(3,c) = ({03} dot s(0,c)) ^ s(1,c) ^ s(2,c) ^ ({02} dot s(3,c))

	/*
	unsigned char temp[16];
	int i = -1;
	int j = -1;
	while (++i < 4)
	{
		unsigned char a[4];
		unsigned char b[4];

		int k = -1;
		while (++k < 4)
		{
			a[k] = state[i * 4 + k];
			b[k] = a[k] << 1;
			if (a[k] & 0x80)
				b[k] = b[k] ^ 0x1b;
		}

		temp[i * 4 + 0] = b[0] ^ a[3] ^ a[2] ^ b[1] ^ a[1];
		temp[i * 4 + 1] = b[1] ^ a[0] ^ a[3] ^ b[2] ^ a[2];
		temp[i * 4 + 2] = b[2] ^ a[1] ^ a[0] ^ b[3] ^ a[3];
		temp[i * 4 + 3] = b[3] ^ a[2] ^ a[1] ^ b[0] ^ a[0];
	}
	i = -1;
	while (++i < 16)
		state[i] = temp[i];
	*/

	unsigned char temp[16];
	for (int i = 0; i < 4; i++)
	{
		unsigned char* a = state + i*4;
		unsigned char* b = temp + i*4;

		b[0] = GMul2[a[0]] ^ GMul3[a[1]] ^ a[2] ^ a[3];
		b[1] = a[0] ^ GMul2[a[1]] ^ GMul3[a[2]] ^ a[3];
		b[2] = a[0] ^ a[1] ^ GMul2[a[2]] ^ GMul3[a[3]];
		b[3] = GMul3[a[0]] ^ a[1] ^ a[2] ^ GMul2[a[3]];

	}
	for (int i = 0; i < 16; i++)
		state[i] = temp[i];
}

/// <summary>
/// Performs inverse column mix operation
/// </summary>
/// <param name="state">State to mix columns on</param>
void AES::invMixColumns(unsigned char* state)
{
	unsigned char temp[16];
	for (int i = 0; i < 4; i++)
	{
		unsigned char* a = state + i * 4;
		unsigned char* b = temp + i * 4;

		b[0] = GMul14[a[0]] ^ GMul11[a[1]] ^ GMul13[a[2]] ^ GMul9[a[3]];
		b[1] = GMul9[a[0]] ^ GMul14[a[1]] ^ GMul11[a[2]] ^ GMul13[a[3]];
		b[2] = GMul13[a[0]] ^ GMul9[a[1]] ^ GMul14[a[2]] ^ GMul11[a[3]];
		b[3] = GMul11[a[0]] ^ GMul13[a[1]] ^ GMul9[a[2]] ^ GMul14[a[3]];

	}
	for (int i = 0; i < 16; i++)
		state[i] = temp[i];
}

/// <summary>
/// Adds round keys to state
/// </summary>
/// <param name="state">State to add round key to</param>
void AES::addRoundKey(unsigned char* state, unsigned char* keys)
{
	for (int i = 0; i < 4 * Nb; i++)
		state[i] = state[i] ^ keys[i];
}



bool AES::encrypt(unsigned char* plaintext, int plaintextLength, unsigned char* ciphertext) // Ciphertext length assumed >= plaintextLength, plaintext padded with null
{
	if (!keyValid)
		return false;

	bool success = true;

	unsigned char* state = new unsigned char[16];

	int iTotal = 0;
	int block = 0;
	while (iTotal < plaintextLength)
	{
		for (int i = 0; i < 16; i++)
		{
			if (iTotal < plaintextLength)
				state[i] = plaintext[iTotal];
			else
				state[i] = 0x00;
			iTotal++;
		}

		if (!encryptBlock(state, ciphertext+(block*16)))
			success = false;

		block++;
	}

	return success;
}

bool AES::decrypt(unsigned char* ciphertext, int ciphertextLength, unsigned char* plaintext) // Plaintext length assumed >= ciphertextLength, fails if cipher length mod 16 != 0
{
	if (!keyValid)
		return false;
	if (ciphertextLength%16 != 0)
		return false;

	bool success = true;

	unsigned char* state = new unsigned char[16];

	int iTotal = 0;
	int block = 0;
	while (iTotal < ciphertextLength)
	{
		for (int i = 0; i < 16; i++)
		{
			state[i] = ciphertext[iTotal];
			iTotal++;
		}

		if (!decryptBlock(state, plaintext + (block * 16)))
			success = false;

		block++;
	}

	return success;
}

void AES::expandKey() // Expands the preset key into expandedKey
{
	int i = 0;

	while (i < keyLength)
	{
		expandedKey[i] = key[i];
		i++;
	}

	unsigned char* temp = new unsigned char[4];

	while (i < 4 * Nb * (Nr + 1))
	{
		int Nk4 = Nk*4;

		for (int j = 0; j < 4; j++)
			temp[j] = expandedKey[i - 4 + j];

		if ((i%Nk4) == 0)
		{
			temp = subWord(rotWord(temp));
			temp[0] = temp[0] ^ Rcon[i/Nk4];
		}
		else if (Nk > 6 && ((i/4)%Nk == 4))
		{
			temp = subWord(temp);
		}

		for (int j = 0; j < 4; j++)
			expandedKey[i+j] = expandedKey[i-Nk4+j] ^ temp[j];

		i += 4;
	}
}

unsigned char* AES::rotWord(unsigned char* word) // Performs rotation on 4 byte word
{
	unsigned char temp = word[0];
	word[0] = word[1];
	word[1] = word[2];
	word[2] = word[3];
	word[3] = temp;
	return word;
}

unsigned char* AES::subWord(unsigned char* word) // Perforsm substitution on 4 byte word
{
	for (int i = 0; i < 4; i++)
		word[i] = sBox[word[i]];
	return word;
}

